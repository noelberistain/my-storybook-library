import React from "react"

import { Button } from "./Button"
import { action } from "@storybook/addon-actions"

export default {
  title: "Material Button",
  component: Button,
}

const Template = (args) => <Button {...args} />

export const Default = Template.bind({})
Default.args = {
  color: "default",
  label: "Default",
  size: "medium",
  variant: "contained",
  onClick: action("Default button clicked"),
}

export const Primary = Template.bind({})
Primary.args = {
  ...Default.args,
  color: "primary",
  label: "Primary",
  onClick: action("Primary button clicked"),
}

export const Secondary = Template.bind({})
Secondary.args = {
  ...Default.args,
  color: "secondary",
  label: "Secondary",
  onClick: action("Secondary button clicked"),
}
