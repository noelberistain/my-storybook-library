import React from "react"
import PropTypes from "prop-types"
import { Button as MUIButton } from "@material-ui/core"

export const Button = (props) => {
  const { color, label, size, variant, ...rest } = props
  return (
    <MUIButton {...rest} color={color} size={size} variant={variant}>
      {label}
    </MUIButton>
  )
}

Button.propTypes = {
  /** Color string from material ui docs */
  color: PropTypes.string,
  /** Required label for a button */
  label: PropTypes.string.isRequired,
}

Button.defaultProps = {
  color: "default",
  label: "Button",
  size: "medium",
}
