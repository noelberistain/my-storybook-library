import React, { useState } from "react"
import PropTypes from "prop-types"

import { TextField as MUITextField } from "@material-ui/core"

const formatPhoneNumber = (phoneNumber) => {
  var match = phoneNumber.match(/^(\d{3})(\d{3})(\d{4})$/)
  if (match) {
    return "(" + match[1] + ") " + match[2] + "-" + match[3]
  }
}

export const TextField = (props) => {
  const { label, variant, ...rest } = props

  const [helperText, setHelperText] = useState("")
  const [input, setInput] = useState("")
  const [error, setError] = useState(false)

  const handleChange = (event) => {
    setInput(event.target.value)

    if (event.target.value.length < 10 && !isNaN(event.target.value)) {
      setError(false)
    } else if (event.target.value.length === 10) {
      setInput(formatPhoneNumber(event.target.value))
    } else {
      setError(true)
      setHelperText("You should type only numbers")
    }
  }

  return (
    <MUITextField
      error={error}
      helperText={error && helperText}
      label={label}
      variant={variant}
      value={input}
      onChange={handleChange}
      {...rest}
    />
  )
}

TextField.propTypes = {
  /** Label as description */
  label: PropTypes.string.isRequired,
  /** Variant for component */
  variant: PropTypes.string.isRequired,
}

TextField.defaultProps = {
  label: "Phone number.",
  variant: "outlined",
}
