import React from "react"

import { TextField } from "./TextField"

export default {
  title: "Validate Phone Number Input TextField",
  component: TextField,
}

const Template = (args) => <TextField {...args} />

export const Default = Template.bind({})
